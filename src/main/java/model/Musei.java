package model;

public class Musei {

    private long museiId;
    private String nomeM;
    private String citta;

    public Musei(){}

    public long getMuseiId() {
        return museiId;
    }

    public void setMuseiId(long museiId) {
        this.museiId = museiId;
    }

    public String getNomeM() {
        return nomeM;
    }

    public void setNomeM(String nomeM) {
        this.nomeM = nomeM;
    }

    public String getCitta() {
        return citta;
    }

    public void setCitta(String citta) {
        this.citta = citta;
    }

    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder();

        builder.append("ID museum: ");
        builder.append(museiId);
        builder.append(" | Museum name: ");
        builder.append(nomeM);
        builder.append(" | City: ");
        builder.append(citta);

        return builder.toString();
    }
}
