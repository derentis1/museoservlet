package servlet;

import dao.MuseiDao;
import dao.MuseiDaoImpl;
import model.Musei;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ReadMuseo extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {

        long id = Long.parseLong(req.getParameter("id"));



        try {
            PrintWriter pw = resp.getWriter();

            if (id > 0){

                MuseiDao museiDao = new MuseiDaoImpl();
                Musei musei = museiDao.findById(id);
                //System.out.println(musei.toString());

                if (musei == null){
                    pw.println("Museo non esiste");
                } else {
                    pw.println("<html>");
                    pw.println("<body>");
                    pw.println("<p> ID: " + musei.getMuseiId() + "</p>");
                    pw.println("<p> Name: " + musei.getNomeM() + "</p>");
                    pw.println("<p> City: " + musei.getCitta() + "</p>");
                    pw.println("</body>");
                    pw.println("</html>");
                }

            } else {
                pw.println("L`id non e' valido, inserire un id maggiore di 0");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
