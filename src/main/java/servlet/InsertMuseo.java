package servlet;

import dao.MuseiDao;
import dao.MuseiDaoImpl;
import model.Musei;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

//@WebServlet("/InsertMuseo") annotation serve per fare configurazione in automatico senza toccare web.xml
public class InsertMuseo extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        resp.setContentType("text/html");
        RequestDispatcher dispatcher;
        PrintWriter pw = resp.getWriter();

        try {
            Musei musei = new Musei();
            MuseiDao museiDao = new MuseiDaoImpl();

            musei.setMuseiId(Long.parseLong(req.getParameter("id")));
            musei.setNomeM(req.getParameter("name"));
            musei.setCitta(req.getParameter("citta"));

            if (museiDao.create(musei)){
                dispatcher = req.getRequestDispatcher("/WEB-INF/success.html");
                dispatcher.forward(req, resp);
            } else {
                pw.println("L`id gia presente nella lista");
                dispatcher = req.getRequestDispatcher("/index.html");
                dispatcher.include(req, resp);
            }

            /*pw.println("<html>");
            pw.println("<body>");
            pw.println("<p> Operazione andata a buon fine </p>");
            pw.println("</body>");
            pw.println("</html>");*/

           /*dispatcher = req.getRequestDispatcher("/WEB-INF/success.html");
            dispatcher.forward(req, resp); //questa riga invia request e response sulla pagina per essere elaborata
            //dispatcher.include(req, resp);*/

        } catch (Exception e) {
            System.out.println("Operazione fallita");
            e.printStackTrace();
        }


    }

}
