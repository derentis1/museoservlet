package dao;

import model.Musei;

import java.util.List;

public interface MuseiDao {

    public boolean create(Musei musei);

    public List<Musei> getAllMusei();

    public Musei findById(long id);

    public void updateMuseum(Musei musei);

    public void deleteMuseum(Musei musei);

}
